s6 - a process supervision suite
--------------------------------

 s6 is a suite of programs designed to allow process
supervision and management. It can also be used as the
foundation for a complete init system: the s6-rc and
s6-linux-init package, for instance, expand on it.

 See https://skarnet.org/software/s6/ for details.


## Installation
  ------------

 See the INSTALL file.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-rc/s6.git && 
cd s6 && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

```
sudo apt install git-buildpackage dpkg-dev libexecline-dev skalibs-dev
```

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

These flags will need to be added to the `debian/rules` file.



* Contact information
  -------------------

 Laurent Bercot <ska-skaware at skarnet.org>

 Please use the <skaware at list.skarnet.org> mailing-list for
questions about the inner workings of s6, and the
<supervision at list.skarnet.org> mailing-list for questions
about process supervision, init systems, and so on.
